"""
Exercise warmup1
-----------------

Simple exercises to get used to python basics.

- From http://introtopython.org/var_string_num.html , do the following:
  - hello world
  - one variable, two messages
  - first name cases
  - full name
  - arithmetic

"""

# Write your solution here
#Hello world
a="Hello world"
print(a)

#one variable, two messages
a='This is my first message'
print(a)
a='This is another message'
print(a)

#first name cases
#name is all the time in lower letters
name='inaki'
print(name)
print(name.title())
print(name.upper())

#full name
#fullname.capitalize() just capitalize the name and fullname.title() will capitalize all words
name='inaki'
surname1='de'
surname2='diego'
name_caps=name.capitalize()
surname2_caps=surname2.capitalize()
fullname=name_caps + ' ' + surname1 + ' ' + surname2_caps
print(fullname)

#Arithmetic
a=4+4
b=10-2
c=4*2
d=16//2
e=2**3
print(a,'=',b,'=',c,'=',d,'=',e)