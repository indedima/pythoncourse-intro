"""
Exercise warmup2
-----------------

Simple exercises to get used to python basics (continued).

- From http://introtopython.org/lists_tuples.html , do the following:
  - first list
  - working list
  - starting from empty
  - ordered numbers (ignore the requirement of using for loops)
  - alphabet slices
  - first twenty

- And *also* the following exercise on dictionaries:
  Create the following dictionary to be used as a phonebook:

        phonebook = { "John" : 7566,  "Jack" : 7264,  "Jill" : 2781}

  Add "Jake" to the phonebook with the phone number 938273443,  and remove Jill
  from the phonebook. Then print the phonebook in alphabetical order
"""

# Write your solution here

# first list
a = ['python', 'c', 'java']
print(a[0])
print(a[1])
print(a[2])

# working list
jobs = ['programmer', 'scientist', 'journalist', 'nurse']
# print(jobs)
present = jobs.index('nurse')
if present:
    print("Nurse is one of our jobs")
jobs.append('driver')
# print(jobs)
jobs.insert(0, 'farmer')
# print(jobs)
for job in jobs:
    print(job.title())

# starting from empty
jobs2 = []
jobs2.append('farmer')
jobs2.append('programmer')
jobs2.append('scientist')
jobs2.append('journalist')
jobs2.append('nurse')
jobs2.append('driver')
print(jobs2[0].title())
print(jobs2[-1].title())

# ordered numbers (ignore the requirement of using for loops)
n = [3, 2, 6, 99, 80]
print("Original order ", n)
print("Ascending order ", sorted(n))
print("Original order ", n)
print("Descending order ", sorted(n, reverse=True))
print("Original order ", n)
print("Reversed order from start", n[::-1])
print("Original order ", n)
n.sort()
print("Permanently increasing order list ", n)
n.sort(reverse=True)
print("Permanently decreasing order list ", n)

# alphabet slices
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'h']
first_letters = alphabet[0:3]
for i in first_letters:
    print(i)
middle_letters = alphabet[3:6]
for i in middle_letters:
    print(i)
last_letters = alphabet[6:]
for i in last_letters:
    print(i)

# first twenty
print(list(range(20)))

# dictionary to be used as a phonebook
phonebook = {"John": 7566, "Jack": 7264, "Jill": 2781}
# print(phonebook)
phonebook.update({"Jake": 938273443})
# print(phonebook)
phonebook.pop("Jill")
# print(phonebook)
for i, j in sorted(phonebook.items()):
    print(i, ":", phonebook[i])