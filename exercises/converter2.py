#!/usr/bin/env python

# solution by cpascual@cells.es

"""
Exercise: converter2
---------------------

Same as exercise converter1 but this time you need to parse the header
to obtain the values for the gain and the offset from there.

Also, you cannot assume that the header has just 3 lines or that the order
of the items in the header is always the same.

In summary: you know that the "gain" is the number just after the word "GAIN"
and the offset is the number after word "OFFSET". You also know that the word
"DATA" marks the end of the header and that after it it comes the channels
data.

If possible, try not to use `for i in len(range(...))`. Use "enumerate" and
"zip" instead.

Note: call the output of this program "converter2.dat", and compare it
(visually) with "converted1.dat"

Important: do not import modules. Do it with the basic python built-in
functions.

Tips:

- see tips of exercise converter1 and also
- note that you can find elements in a list using the .index() method of a list

"""

# Write your solution here
with open('sp8c.dat', "r") as f:
    f2 = f.read()
    list1 = f2.split()
    GAIN = float(list1[list1.index('GAIN') + 1])  # value of the item following the one returned by .index()
    OFFSET = float(list1[list1.index('OFFSET') + 1])
print(GAIN)
print(OFFSET)
list1 = [float(number) for number in list1[list1.index('DATA') + 1:]]  # header finishes with the str 'DATA'
m = max(list1)  # maximum number
counts = [i / m for i in list1]  # normalize the list
times = [(channel * GAIN + OFFSET) for channel, count in enumerate(counts)]  # avoiding range(len(list))
with open("converted2.dat", "w") as nf:  # open output file for writing, "with" statement
    for t, c in zip(times, counts):  # use of zip, avoid range(len(counts))
        nf.write("{}\t{}\n".format(t, c))
